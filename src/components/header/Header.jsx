import React, { useRef, useEffect,useState} from 'react';

import { Link, useLocation, useNavigate} from 'react-router-dom';

import jwt_decode from 'jwt-decode'

import './header.scss';

import GoogleImg from '../../assets/google.png';
import Github from '../../assets/github.png';
import Facebook from '../../assets/facebook.png';

import { MovieSearch } from '../movie-grid/MovieGrid';
import FormInput from '../form-input/FormInput'
import Modal from 'react-modal';

// import Button from '../button/Button';





const inputs =  [
    {
      id: 1,
      name: "username",
      type: "text",
      placeholder: "Username",
      label: "Username",
      
    },
   
    {
      id: 2,
      name: "password",
      type: "password",
      placeholder: "Password",
      label: "Password",
   
    },
   
  ];

  const inputsRegister =  [
    {
      id: 1,
      name: "username",
      type: "text",
      placeholder: "Username",
      label: "Username",
      
    },
    {
      id: 2,
      name: "email",
      type: "email",
      placeholder: "Email",
  
      label: "Email",
      
    },
   
    {
      id: 3,
      name: "password",
      type: "password",
      placeholder: "Password",
      label: "Password",
      
    },
    {
      id: 4,
      name: "confirmPassword",
      type: "password",
      placeholder: "Confirm Password",
      label: "Confirm Password",
  
      
    },
  ];





const headerNav = [
    {
        display: 'Home',
        path: '/home'
    },
    {
        display: 'Movies',
        path: '/movie'
    },
    // {
    //     display: 'TV Series',
    //     path: '/tv'
    // }
];


const Header = () => {

  const [user, setUser] = useState({})

  const navigate = useNavigate("/home")

  let handleCallbackResponse = (response) => {
    let userObject = jwt_decode(response.credential)
    console.log(response.credential);
    setUser(userObject)
    document.getElementById('signInDiv').hidden = true
    // window.location.href = "http://localhost:3000/home"
  };

  useEffect(() => {
    window.google?.accounts.id.initialize({
      client_id: "638538265910-hrr5vq8cugv9ku1l9kqprokgplod6oh0.apps.googleusercontent.com",
      callback: handleCallbackResponse,
      // login_uri : "http://localhost:3000/home",
      // ux_mode: "redirect"
      
    }, navigate)
    
    window.google?.accounts.id.renderButton(document.getElementById("signInDiv"), {
      theme: "outline",
      size: "large",
      
    });

    window.google.accounts.id.renderButton(document.getElementById("signInDiv2"), {
      theme: "outline",
      size: "large",
    });

  
    window.google?.accounts.id.prompt()
  },[])


  
  

  const handleLogout = (e) => {
    setUser({})
    document.getElementById('signInDiv').hidden = false
    window.location.reload();
  }
  

    const { pathname } = useLocation();
    const headerRef = useRef(null);

    // const toLogin = '/login'
    // const toRegister = '/register'
    const active = headerNav.findIndex(e => e.path === pathname);

    const [isOpen, setIsOpen] = useState(false)

    const [isOpens, setIsOpens] = useState(false)

    function toggleLogin() {
        setIsOpen(!isOpen);
    }
    function toggleRegister() {
        setIsOpens(!isOpens);
    }

    useEffect(() => {
        const shrinkHeader = () => {
            if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
                headerRef.current.classList.add('shrink');
            } else {
                headerRef.current.classList.remove('shrink');
            }
        }
        window.addEventListener('scroll', shrinkHeader);
        return () => {
            window.removeEventListener('scroll', shrinkHeader);
        };
    }, []);

    return (
        <div ref={headerRef} className="header">
            <div className="header__wrap container">
                <div className="logo">
                    <Link to="/">Rectflix</Link>
                </div>
                <ul className="header__nav">
                <MovieSearch/>
               
                {
                
                Object.keys(user).length !== 0 &&
                // <button onClick={handleLogout}>Sign Out</button>
                
                <button onClick={handleLogout} style={{padding:"0.5rem 1.5rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"transparent",color:"#fff",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>Logout</button>
                }
                {user && (
                  <div>
                    <p>{user.name}</p>
                 
                  </div>
               )}
                </ul>

                  


                {
                        (pathname === "/")
                        ?
                        <span style={{display:"inline"}}>
                          <div  id="signInDiv"></div>
                          <button onClick={toggleLogin} style={{margin:"1rem",padding:"0.5rem 1.5rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",color:"#fff",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>Login</button>
                          <button onClick={toggleRegister} style={{padding:"0.5rem 1.5rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"transparent",color:"#fff",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>Register</button>
                        </span>
                        :
                        headerNav.map((e, i) => (
                          <ul className="header__nav">
                            <li key={i} className={`${i === active ? 'active' : ''}`}>
                                <Link to={e.path}>
                                    {e.display}
                                </Link>
                            </li>
                                 {/* <button onClick={handleLogout}>Sign Out</button> */}
                            
                          </ul>
                          
                        ), )
                    }

                    

                    

                  
                    
            </div>



            <Modal
                isOpen={isOpens}
                onRequestClose={toggleRegister}
                contentLabel="My dialog"
                className="mymodal"
                overlayClassName="myoverlay"
                closeTimeoutMS={500}
                appElement={document.getElementById('root') || undefined}
            >
           <form >
           <button onClick={toggleRegister}>Close modal</button>
                <h1>Register</h1>
                {inputsRegister.map((input) => (
                <FormInput
                    key={input.id}
                    {...input}
                    // value={values[input.name]}
                    // onChange={onChange}
                />
                ))}
               <button style={{padding:"0.5rem 1.5rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",color:"#fff",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>Login</button>
               <p style={{marginTop:"1rem"}}>Or Login With</p>
               <div style={{textAlign:"center"}}>
                <button style={{padding:"0.2rem 0.8rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>
                    <img src={Github} alt="" />
                </button>
                <button style={{padding:"0.2rem 0.8rem",marginLeft:"1.8rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>
                    <img src={Facebook} alt="" />
                </button>
                <button style={{padding:"0.2rem 0.8rem",marginLeft:"1.8rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>
                    <img src={GoogleImg} alt="" />
                </button>
               </div>
            </form>
           
      </Modal>


        <Modal
                isOpen={isOpen}
                onRequestClose={toggleLogin}
                contentLabel="My dialog"
                className="mymodal"
                overlayClassName="myoverlay"
                closeTimeoutMS={500}
                appElement={document.getElementById('root') || undefined}
            >
           <form >
           <button onClick={toggleLogin}>Close modal</button>
                <h1>Login</h1>
                {inputs.map((input) => (
                <FormInput
                    key={input.id}
                    {...input}
                    // value={values[input.name]}
                    // onChange={onChange}
                />
                ))}
               <button style={{padding:"0.5rem 1.5rem", borderRadius:"30px",marginTop:"1rem",border:"4px solid transparent",backgroundColor:"#ff0000",color:"#fff",fontWeight:"600",boxShadow:"0px 0px 7px 8px rgba(255, 0, 0, 0.3019607843)"}}>Login</button>
               <p style={{marginTop:"1rem"}}>Or Login With</p>
               <div style={{textAlign:"center"}}>
             
                <div style={{marginTop: "1rem"}} id="signInDiv"></div>
                <div id="signInDiv2"></div>
               </div>
                
            </form>
           
      </Modal>

        </div>



        
    );
   
}

export default Header;
