const apiConfig = {
    baseUrl: 'https://api.themoviedb.org/3/',
    apiKey: 'ecb4f95925056117fe020a3233ea222e',
    originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}

export default apiConfig;